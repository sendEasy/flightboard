# file:features/steps/Flights_API_Steps.py
# ----------------------------------------------------------------------------
# STEPS:
# ----------------------------------------------------------------------------
from behave import *
from bs4 import BeautifulSoup
import requests
import requests

try:
	response = requests.get('http://127.0.0.1:5000/api/v1/flights/3')
except requests.exceptions.ConnectionError as connectionError:
	raise SystemExit("Application Is Not Running",connectionError)

data = response.json()

@when(u'I retrieve flight.id 3')
def impl(context):
	context.page = requests.get('http://127.0.0.1:5000/api/v1/flights/3').content
@then(u'I should get a status "200"')
def impl(context):
	assert (response.status_code == 200)
@then(u'the airport_city should be "London"')
def airport_city_should_be_london(context):
    assert (data['airport_city'] == 'London')
