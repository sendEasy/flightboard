# file:features/sender.feature
Feature: Flight Endpoint
	As a Flightsboard User
	I want to see all Flight
	So that I can decide from where I am sending my parcel from

Scenario: Retrieve sender point id 5
    When I retrieve Flight.id 3
    Then I should get a status "200"
	And the airport_city should be "London"