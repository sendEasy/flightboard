from unittest import TestCase
from api import Flights

class TestValideFlight(TestCase):

    def test_valid_flight(self):
        valid_flight = {
            'id': 1,
            'flight_code': 'AZ123',
            'flies_from': 'Rome',
            'flies_to': 'London',
            'company_name': 'Alitalia',
            'flight_type': 'Arrival',
            'flight_status': 'Landed',
            'time_expected': '19:00',
            'airport_code': 'HTW',
            'airport_city': 'London',
            'airport_country': 'United Kingdom'
        }

        self.assertEqual(Flights.validate_flight_object(valid_flight), True)

    def test_missing_flight_code(self):
        missing_flight_code = {
            'id': 1,
            'flies_from': 'Rome',
            'flies_to': 'London',
            'company_name': 'Alitalia',
            'flight_type': 'Arrival',
            'flight_status': 'Landed',
            'time_expected': '19:00',
            'airport_code': 'HTW',
            'airport_city': 'London',
            'airport_country': 'United Kingdom'
        }

        self.assertEqual(Flights.validate_flight_object(missing_flight_code), False)

    def test_empty_flight(self):
        empty_flight = {}

        self.assertEqual(Flights.validate_flight_object(empty_flight), False)