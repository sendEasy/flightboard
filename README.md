# FLIGHTBOARD #

# DEMO #
For the purpose of this Demo you can simply check the Flightboard departures and arrivals endpoints.

* Departures: https://demoflightboard.herokuapp.com/flightsboard/departures
* Arrivals: https://demoflightboard.herokuapp.com/flightsboard/arrivals

# API #
You can also interact with the Flights Rest API [http://demoflightboard.herokuapp.com/api/v1/flights] that where you can:

* Get a Flight.
* Get all Flights.
* Create a Flight.
* Delete a Flight.
* Edit (PATCH) a flight.
* Replace (PUT) a flight.

### How do I Interact with the Api ###

* To Play with the API you can Install Postman 
* Download Postman Flight baord API Colelction: info/postman/Flight Controller API.postman_collection.json
* Import the Collection into Postman


### Flights API Centre ###

#### Create Flight

* In this DEMO there are 2 types of **flight_type** that can be used:

1. ```Departure```
2. ```Arrival```

* In this DEMO there are 8 types of **flight_status** that can be used:

1. ```Last Call```
2. ```Arrival```
3. ```On Schedule```
4. ```Delayed```
5. ```Cancelled```
6. ```Gate Open```
7. ```Boarding```
8. ```Landed```


```curl
curl --request POST \
  --url http://demoflightboard.herokuapp.com/api/v1/flights \
  --header 'Content-Type: application/json' \
  --data '    {
  	"flight_code":"MJ389",
  	"flies_from":"London",
    "flies_to":"Paris",
    "company_name":"Moon Jet",
    "flight_type":"Departure",
    "flight_status": "Last Call",
    "time_expected":"9:35",
    "airport_code":"HTW",
    "airport_city":"London",
    "airport_country":"United Kingdom"
  }'
```

#### Update Flight (Put)

```curl
curl --request PUT \
  --url http://demoflightboard.herokuapp.com/api/v1/flights/{flight_id} \
  --header 'Content-Type: application/json' \
  --data '  {
  	"flight_code":"AZ234",
  	"flies_from":"Turin",
    "flies_to":"London",
    "company_name":"Alitalia",
    "flight_type":"Arrival",
    "flight_status": "On Schedule",
    "time_expected":"11:28",
    "airport_code":"HTW",
    "airport_city":"London",
    "airport_country":"United Kingdom"
  }
'
```

#### Update Flight (Patch)

```curl
curl --request PATCH \
  --url http://demoflightboard.herokuapp.com/api/v1/flights/{flight_id} \
  --header 'Content-Type: application/json' \
  --data ' {
        "company_name":"Swiss Air",
        "flight_code":"SA128",
        "flies_to":"Zurich",
        "time_expected":"17:30"
     }
'
```

#### Get Flight

```curl --request GET   --url 'http://demoflightboard.herokuapp.com/api/v1/flights/{flight_id}'```

#### Get All Flights

```curl --request GET   --url 'http://demoflightboard.herokuapp.com/api/v1/flights'```


#### Delete a Flight

```curl 
curl --request DELETE \
  --url http://demoflightboard.herokuapp.com/api/v1/flights/{flight_id} \
  --header 'Content-Type: application/json'
```


---

---

## Setup Local environment

This Document assumes that all the developers will be using Windows >7.

When you're done, you can star-up Flightboard app in your local environment and interact with the API on your machine, run test and change code if necessary.



## Install Python >= 3.7 version and pip


1. Download the latest python installation file from [the official website](https://www.python.org/downloads/windows/) (3.7 is desirable) and Install it.
2. Navigate to C:\path\to\pythonxx\Scripts and installation and run via cmd (as Administrator) the command ``` python get-pip.py ```  to install pip.
3. Set Environment Variables in Windows:  ***C:\PythonXX and*** and  ***C:\PythonXX\Scripts*** and restart your machine to run both python and pip dynamically.
4. Run ```python --version``` to check if the installation worked.
6. To chek pip just run ```pip --version``` or ```python -m pip --version``` alternatively.
7. Laslty run ```pip freeze``` or ```python -m pip freeze``` to verify which packages we have available.

---

## Virtualenv to contain the app

1. Navigate to the flightboard main app directory ```your_Home\{{$yourRepository}}\flightboard```.
2. Install ***virtualenv*** via cmd (as Administrator) with the command ``` pip install virtualenv```.
3. Install ***virtualenv-win*** via cmd (as Administrator) with the command ```pip install virtualenvwrapper-win```.
4. Set the virtualenv home directory in your system variables to the desired path; best practise is to create a ```venv``` directory in your python project firt; set ```WORKON_HOME``` to ```your_Home\{{$yourRepository}}\flightboard\venv```.
5. Run the Command ```mkvirtualenv flightboard_playground``` to create a local environment where to run flightboard API.
6. Run ```setprojectdir .``` to bind the virtualenv with the current API-Demo directory.
7. Run ```pip install -r requirements.txt``` - this file contains all the packages needed for this app to run.

Useful Commands:
```lsvirtualenv``` to list all virtual environments available/created.
```workon flightboard_playground``` to actually work on an isolated python environment.
```deactivate``` to actually come off the virtual environment selected.
```rmvirtualenv flightboard_playground``` to delete the virtual environment from your python enviroments.


---


## Postgreql

After you have installed Postgres locally and you are able to run its commands from CLI or win Command Line then do the followings:
 * Setup environmental variables and add variable ```PGDATA``` with the value of ```path:\to\PostgreSQL\$version\data```. 
 * Add to ```PATH``` these two values  ```path:\to\PostgreSQL\$version\lib``` and   ```path:\to\PostgreSQL\$version\bin```.
 * To start the local Postgres server type```pg_ctl status``` to check status & ```pg_ctl start``` to start the service.
 * Run the command ```psql postgres postgres``` to login in the postgres db as postgres user.
 * Run the Command ```ALTER USER postgres WITH ENCRYPTED PASSWORD 'yourPassword';``` to set your password;
 * Create the flightboard DB with ```CREATE DATABASE flightboard;```

To Connect to flightboard DB:
1. Type ```psql flightboard postgres``` to connect
2. And then type ```\conninfo``` to check the DB URI that will be used in ```flightboard.py```

### Migration and Alembic

Once your setup is done - it is time to create the table/object in the database.

1. Navigate to ```flightboard\app```   where the ```manage.py``` file is.
2. Run ```python manage.py db init``` - this will create all the folders and ini files for the  model migrations into te DB.
3. ```python manage.py db migrate``` - to create a migration file in the ```migrations``` folder under the path ```flightboard\app\cmd\migrations``` and the script to upgrade the DB.
4. ```python manage.py db upgrade``` - Finally this command will implement your changes in the db.
5. ```python manage.py db downgrade``` - This command can be used to rollback at initial DB state.


### After the first migration you can simply follow the steps below
1. ```python manage.py db migrate``` 
2. ```python manage.py db upgrade``` or ```python manage.py db dowgrade```

Reference: https://www.compose.com/articles/schema-migrations-with-alembic-python-and-postgresql/

---
### Run flightboard on Heroku

After you have created an account on heroku or you have been added to an existing app repo: ```heroku login```

Create an application on Heroku: ```heroku create demoflightboard```

After Created copy the git url and add it to the git locally: ```git remote add prod https://git.heroku.com/flightboard.git```

Set Env Variables for app settings: ```heroku config:set APP_SETTINGS=config.ProductionConfig --app demoflightboard```

Set the CollecrStatics to false to avoid build issues: ```heroku config:set DISABLE_COLLECTSTATIC=1```

Create a Postgres instance on heroku linked to your app: ```heroku addons:create heroku-postgresql:hobby-dev --app demoflightboard```

Build on the Heroku server: ```git push prod master```

Run the migration on Heroku: ```heroku run python app/manage.py db upgrade --directory app/migrations --app demoflightboard```

Or run script directly ```heroku run python app/migrations/versions/a139233921df_.py db upgrade --directory app/migrations --app demoflightboard```

To Start the Application: ```ps:scale web=1 --app demoflightboard```

To Stop the Application: ```ps:scale web=0 --app demoflightboard```

#### Useful Commands

```heroku config --app demoflightboard```

```pg:info --app demoflightboard```

```heroku pg:psql --app demoflightboard```

```heroku apps:info --app demoflightboard```

```heroku buildpacks:clear --app demoflightboard```

```heroku buildpacks:add --index heroku/python --app demoflightboard```

---
## Run flightboard Locally

Type ```python flightboard.py```

You should see the below log entries in the local terminal:
```bash

(flightboard_playground) C:\Repositories\flightboard\app>python flightboard.py
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 286-767-996
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
```


## Run BDD Tests

While the app is Running

1. Type ```cd path/to/flightboard/test```
2. And then type ```behave features```


### What is this repository for? ###

* Demo FlightBoard for Xero Interview process.
* 1.0

### Who do I talk to? ###

* Franco Cardinali
* fcardinali.uk@googlemail.com