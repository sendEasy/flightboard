import json
from database import db


class Flight(db.Model):
    __tablename__ = 'flights'

    id = db.Column(db.Integer, primary_key=True)
    flight_code = db.Column(db.String, nullable=False)
    flies_from = db.Column(db.String, nullable=False)
    flies_to = db.Column(db.String, nullable=False)
    company_name = db.Column(db.String, nullable=False)
    flight_type = db.Column(db.Enum ('Departure','Arrival', name='flight_type'), nullable=False)
    flight_status = db.Column(db.Enum ('Landed','On Schedule','Delayed','Cancelled','Gate Open','Boarding','Last Call', name='flight_status'), nullable=False)
    time_expected = db.Column(db.String, nullable=False)
    airport_code = db.Column(db.String, nullable=False)
    airport_city = db.Column(db.String, nullable=False)
    airport_country = db.Column(db.String, nullable=False)

    def json(self):
        return {'id': self.id, 'flight_code': self.flight_code, 'flies_from': self.flies_from, 'flies_to': self.flies_to,
                'company_name': self.company_name, 'flight_type': self.flight_type, 'flight_status': self.flight_status,
                'time_expected': self.time_expected, 'airport_code': self.airport_code, 'airport_city': self.airport_city,
                'airport_country': self.airport_country
                }

    def add_flight( _flight_code, _flies_from, _flies_to, _company_name, _flight_type, _flight_status, _time_expected,
                   _airport_code, _airport_city, _airport_country):
        new_flight = Flight(flight_code=_flight_code, flies_from=_flies_from, flies_to=_flies_to,
                            company_name=_company_name, flight_type=_flight_type, flight_status=_flight_status, time_expected=_time_expected,
                            airport_code=_airport_code, airport_city=_airport_city, airport_country=_airport_country)
        db.session.add(new_flight)
        db.session.flush()
        new_flight_id = new_flight.id
        db.session.commit()
        return new_flight_id

    @staticmethod
    def get_all_flights():
        return [Flight.json(flight) for flight in Flight.query.all()]

    def get_flight(_id):
        return Flight.json(Flight.query.filter_by(id=_id).first())

    def delete_flight(_id):
        is_succesfull = Flight.query.filter_by(id=_id).delete()
        db.session.commit()
        return bool(is_succesfull)

    def update_flight_code(_id, _flight_code):
        flight_to_update = Flight.query.filter_by(id=_id).first()

        if _flight_code:
            flight_to_update.flight_code = _flight_code
        db.session.commit()

    def update_flight_flies_from(_id, _flies_from):
        flight_to_update = Flight.query.filter_by(id=_id).first()

        if _flies_from:
            flight_to_update.flies_from = _flies_from
        db.session.commit()

    def update_flight_flies_to(_id, _flies_to):
        flight_to_update = Flight.query.filter_by(id=_id).first()

        if _flies_to:
            flight_to_update.flies_to = _flies_to
        db.session.commit()

    def update_flight_company_name(_id, _company_name):
        flight_to_update = Flight.query.filter_by(id=_id).first()

        if _company_name:
            flight_to_update.company_name = _company_name
        db.session.commit()

    def update_flight_type(_id, _flight_type):
        flight_to_update = Flight.query.filter_by(id=_id).first()

        if _flight_type:
            flight_to_update.flight_type = _flight_type
        db.session.commit()

    def update_flight_status(_id, _flight_stauts):
        flight_to_update = Flight.query.filter_by(id=_id).first()

        if _flight_stauts:
            flight_to_update.flight_status = _flight_stauts
        db.session.commit()

    def update_flight_time_expected(_id, _time_expected):
        flight_to_update = Flight.query.filter_by(id=_id).first()

        if _time_expected:
            flight_to_update.time_expected = _time_expected
        db.session.commit()

    def update_flight_airport_code(_id, _airport_code):
        flight_to_update = Flight.query.filter_by(id=_id).first()

        if _airport_code:
            flight_to_update.airport_code = _airport_code
        db.session.commit()

    def update_flight_airport_city(_id, _airport_city):
        flight_to_update = Flight.query.filter_by(id=_id).first()

        if _airport_city:
            flight_to_update.airport_city = _airport_city
        db.session.commit()

    def update_flight_airport_country(_id, _airport_country):
        flight_to_update = Flight.query.filter_by(id=_id).first()

        if _airport_country:
            flight_to_update.airport_country = _airport_country
        db.session.commit()

    def replace_flight(_id, _flight_code, _flies_from, _flies_to, _company_name, _flight_type, _flight_status, _time_expected, _airport_code, _airport_city, _airport_country):
        flight_to_replace = Flight.query.filter_by(id=_id).first()

        flight_to_replace.flight_code = _flight_code
        flight_to_replace.flies_from = _flies_from
        flight_to_replace.flies_to = _flies_to
        flight_to_replace.company_name = _company_name
        flight_to_replace.flight_type = _flight_type
        flight_to_replace.flight_status = _flight_status
        flight_to_replace.time_expected = _time_expected
        flight_to_replace.airport_code = _airport_code
        flight_to_replace.airport_city = _airport_city
        flight_to_replace.airport_country = _airport_country

        db.session.commit()

    def __repr__(self):
        flight_object = {
            'id': self.id,
            'flight_code': self.flight_code,
            'flies_from': self.flies_from,
            'flies_to': self.flies_to,
            'company_name': self.company_name,
            'flight_type': self.flight_type,
            'flight_status': self.flight_status,
            'time_expected': self.time_expected,
            'airport_code': self.airport_code,
            'airport_city': self.airport_city,
            'airport_country': self.airport_country
        }
        return json.dumps(flight_object)