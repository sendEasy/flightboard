#!flask/bin/python
from flask import Flask
import os
from database import db, app
from api.Users import users_api
from api.Flights import flights_api
from api.Home import arrivals,departures


def create_app(app):
    db.init_app(app)
    app.register_blueprint(flights_api)
    app.register_blueprint(users_api)
    app.register_blueprint(arrivals)
    app.register_blueprint(departures)

    return app


if __name__ == '__main__':
    create_app(app).run(host='0.0.0.0', port=int(os.environ.get("PORT", 5000)))
