import datetime, jwt
import os
from functools import wraps
from flask import Blueprint, request, Response, jsonify
from models.user_model import User

users_api = Blueprint('users_api', __name__)


def token_required(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        token = request.args.get('token')
        try:
            jwt.decode(token, os.environ['SECRET_KEY'])
            return f(*args, **kwargs)
        except:
            return jsonify({'error': 'Invalid token Error: Please use a valid token.'}), 401
    return wrapper


@users_api.route('/api/login', methods=['POST'])
def get_token():
    login_request_data = request.get_json()
    username = str(login_request_data['username'])
    password = str(login_request_data['password'])

    match = User.username_password_match(username, password)

    if match:
        expiration_date = datetime.datetime.utcnow() + datetime.timedelta(seconds=100)
        token = jwt.encode({'exp': expiration_date}, os.environ['FLIGHTBOARD_SECRET_KEY'], algorithm='HS256')
        return token
    else:
        return Response('', 401, mimetype='applciation/json')