from flask import Blueprint, render_template
import psycopg2
import os

arrivals = Blueprint('arrivals', __name__)
departures = Blueprint('departures', __name__)

con = psycopg2.connect(os.environ['FLIGHTBOARD_DATABASE_URL'])
cursor = con.cursor()


@arrivals.route('/flightsboard/arrivals', methods=['post', 'get'])
def arrival_page():
    cursor.execute("select * from flights where flight_type='Arrival'")
    result = cursor.fetchall()
    return render_template("arrivals.html", data=result)


@departures.route('/flightsboard/departures', methods=['post', 'get'])
def departure_page():
    cursor.execute("select * from flights where flight_type='Departure'")
    result = cursor.fetchall()
    return render_template("departures.html", data=result)