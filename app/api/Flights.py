import os,jwt
from flask import jsonify, request, Blueprint, Response
from models.flight_model import Flight
from api.Users import *
import json

flights_api = Blueprint('flights_api', __name__)

# GET ALL Flights
@flights_api.route('/api/v1/flights')
def get_flights():
    return jsonify({'flights': Flight.get_all_flights()})


# GET ONE Flight
@flights_api.route('/api/v1/flights/<int:flight_id>')
def get_flight_by_flight_id(flight_id):
    flight = Flight.get_flight(flight_id)
    return jsonify(flight)


# TODO: ????
# Create a Flight
@flights_api.route('/api/v1/flights', methods=['POST'])
#@token_required
def add_flight():
    flight_request_data = request.get_json()
    if validate_flight_object(flight_request_data):

        flightid = Flight.add_flight(
                          flight_request_data['flight_code'],
                          flight_request_data['flies_from'],
                          flight_request_data['flies_to'],
                          flight_request_data['company_name'],
                          flight_request_data['flight_type'],
                          flight_request_data['flight_status'],
                          flight_request_data['time_expected'],
                          flight_request_data['airport_code'],
                          flight_request_data['airport_city'],
                          flight_request_data['airport_country'])

        response = Response("Created a new Flight with id " + str(flightid) , 201, mimetype='application/json')
        response.headers['Location'] = "/api/v1/flights/" + str(flightid)
        response.headers['Status'] = 201

        return response

    else:
        invalid_flight_object_error_msg = {
            "error": "Invalid Flight object payload passed in the request ",
            "helpString": "Data should have been similar to this - {'id':1,"
                          "'flight_code':'AZ102',"
                          "'flies_from':'Rome',"
                          "'flies_to':'London',"
                          "'company_name':'Alitalia',"
                          "'flight_type':'Arrival',"
                          "'flight_status':'On Schedule',"
                          "'time_expected':'18:50',"
                          "'airport_code':'HTW',"
                          "'airport_city':'London',"
                          "'airport_country':'United Kingdom'} "
        }
        response = Response(json.dumps(invalid_flight_object_error_msg), status=400, mimetype='application/json');
        return response


# UPDATE A SENDER
@flights_api.route('/api/v1/flights/<int:flight_id>', methods=['PUT'])
#@token_required
def update_flight_by_sender_id(flight_id):
    update_request_data = request.get_json()
    if not validate_flight_object(update_request_data):
        invalid_flight_object_error_msg = {
            "error": "Invalid Sender object passed in the request ",
            "helpString": "Data should have been similar to this - {'id':1,"
                          "'flight_code':'AZ102',"
                          "'flies_from':'Rome',"
                          "'flies_to':'London',"
                          "'company_name':'Alitalia',"
                          "'flight_type':'Arrival',"
                          "'flight_status':'On Schedule',"
                          "'time_expected':'18:50',"
                          "'airport_code':'HTW',"
                          "'airport_city':'London',"
                          "'airport_country':'United Kingdom'} "
        }
        response = Response(json.dumps(invalid_flight_object_error_msg), status=400, mimetype='application/json');
        return response

    Flight.replace_flight(flight_id, update_request_data['flight_code'], update_request_data['flies_from'],
                          update_request_data['flies_to'], update_request_data['company_name'],
                          update_request_data['flight_type'], update_request_data['flight_status'],
                          update_request_data['time_expected'], update_request_data['airport_code'],
                          update_request_data['airport_city'], update_request_data['airport_country'])

    response = Response("", status=204)
    response.headers['Location'] = "/api/v1/flights/" + str(flight_id)
    return response


# UPDATE A FLIGHT NAME with PATCH
@flights_api.route('/api/v1/flights/<int:flight_id>', methods=['PATCH'])
#@token_required
def patch_flight_by_sender_id(flight_id):
    request_data = request.get_json()

    if "flight_code" in request_data:
        Flight.update_flight_code(flight_id, request_data['flight_code'])

    if "flies_from" in request_data:
        Flight.update_flight_flies_from(flight_id, request_data['flies_from'])

    if "flies_to" in request_data:
        Flight.update_flight_flies_to(flight_id, request_data['flies_to'])

    if "company_name" in request_data:
        Flight.update_flight_company_name(flight_id, request_data['company_name'])

    if "flight_type" in request_data:
        Flight.update_flight_type(flight_id, request_data['flight_type'])

    if "flight_status" in request_data:
        Flight.update_flight_status(flight_id, request_data['flight_status'])

    if "time_expected" in request_data:
        Flight.update_flight_time_expected(flight_id, request_data['time_expected'])

    if "airport_code" in request_data:
        Flight.update_flight_airport_code(flight_id, request_data['airport_code'])

    if "airport_city" in request_data:
        Flight.update_flight_airport_city(flight_id, request_data['airport_city'])

    if "airport_country" in request_data:
        Flight.update_flight_airport_country(flight_id, request_data['airport_country'])

    response = Response("", status=204)
    response.headers['Location'] = "/api/v1/flights/" + str(flight_id)
    return response


# DELETE A Flight
@flights_api.route('/api/v1/flights/<int:flight_id>', methods=['DELETE'])
#@token_required
def delete_flight_by_flight_id(flight_id):
    if (Flight.delete_flight(flight_id)):
        response = Response("", status=204)
        return response

    invalid_flight_object_error_msg = {
        "error": "Flight with id: " + str(flight_id) + " was not found."
    }
    response = Response(json.dumps(invalid_flight_object_error_msg), status=400, mimetype='application/json')
    return response;


# TODO: Add validation:
#  Check if mandatory fields are not Empty.
def validate_flight_object(flight_payload):
    if (
                "flight_code" in flight_payload
            and "flies_from" in flight_payload
            and "flies_to" in flight_payload
            and "company_name" in flight_payload
            and "flight_type" in flight_payload
            and "flight_status" in flight_payload
            and "time_expected" in flight_payload
            and "airport_code" in flight_payload
            and "airport_city" in flight_payload
            and "airport_country" in flight_payload):
        return True
    else:
        return False
